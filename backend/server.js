const express= require("express")
const cors= require("cors")
const routerService= require("./exmservice")
const app= express()
app.use(cors('*'))
app.use(express.json())

app.use("/app",routerService)
app.listen(4000,'0.0.0.0',()=>{
    console.log("your server is started on port 4000")
})